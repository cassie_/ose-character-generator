const rollDice = (number, sides) => {
  const sum = []
  for (let i = 0; i < number; i++) {
    sum.push(dice(sides))
  }

  return sum.reduce((prev, curr) => {
    return prev + curr
  }, 0)
}

const dice = (sides) => {
  return 1 + Math.floor(Math.random() * sides)
}

const alignments = ["Law", "Neutrality", "Chaos"];

const randomElement = (array) => array[Math.floor(Math.random() * array.length)];

const numericModifier = (value) => {
  if (value === 3) {
    return -3;
  }
  if (value >= 4 && value <= 5) {
    return -2;
  }
  if (value >= 4 && value <= 5) {
    return -2;
  }
  if (value >= 6 && value <= 8) {
    return -1;
  }
  if (value >= 9 && value <= 12) {
    return 0;
  }
  if (value >= 13 && value <= 15) {
    return 1;
  }
  if (value >= 16 && value <= 17) {
    return 2;
  }
  if (value === 18) {
    return 3;
  }
};

const generateHp = (value, dice_roll) => {
  const sum = numericModifier(value) + dice_roll;
  return sum > 0 ? sum : 1;
}

export class Character {
  constructor() {
    this.alignment = randomElement(alignments);
    this.attributes = new Attributes();
    this._class = randomClass(this.attributes, this.alignment);
    this.gold = rollDice(3, 6) * 10;
  }
}

class Attributes {
  constructor() {
    this.strength = rollDice(3, 6);
    this.intelligence = rollDice(3, 6);
    this.wisdom = rollDice(3, 6);
    this.dexterity = rollDice(3, 6);
    this.constitution = rollDice(3, 6);
    this.charisma = rollDice(3, 6);
  }
}

class Cleric {
  constructor(attributes, alignment) {
    this.hp = generateHp(attributes.constitution, rollDice(1, 6))
    this.languages = ["Common", alignment]
    this.prime_requisite = "WIS"
    this.class_name = "Cleric"
  }
}

class Fighter {
  constructor(attributes, alignment) {
    this.hp = generateHp(attributes.constitution, rollDice(1, 8))
    this.languages = ["Common", alignment]
    this.prime_requisite = "STR";
    this.class_name = "Fighter"
  }
}

class Magic_User {
  constructor(attributes, alignment) {
    this.hp = generateHp(attributes.constitution, rollDice(1, 4))
    this.languages = ["Common", alignment]
    this.prime_requisite = "WIS";
    this.class_name = "Magic-User"
  }
}

class Thief {
  constructor(attributes, alignment) {
    this.hp = generateHp(attributes.constitution, rollDice(1, 4))
    this.languages = ["Common", alignment]
    this.prime_requisite = "DEX";
    this.class_name = "Thief"
  }
}

const randomClass = (attributes, alignment) => {
  const class_names = ["Fighter", "Cleric", "Magic-User", "Thief"];
  const selection = randomElement(class_names);

  switch (selection) {
    case "Fighter":
      return new Fighter(attributes, alignment);
    case "Cleric":
      return new Cleric(attributes, alignment);
    case "Magic-User":
      return new Magic_User(attributes, alignment);
    case "Thief":
      return new Thief(attributes, alignment);
    default:
      return "Error."
  }
}
