export const rollDice = (number, sides) => {
  const sum = []
  for (let i = 0; i < number; i++) {
    sum.push(dice(sides))
  }

  return sum.reduce((prev, curr) => {
    return prev + curr
  }, 0)
}

const dice = (sides) => {
  return 1 + Math.floor(Math.random() * sides)
}