import React from "react";

export const IntelligenceModifier = ({ value }) => {
  return (
    <div className="modifier">
      <p>Spoken languages: {languages(value)}</p>
      <p>Literacy: {literacy(value)}</p>
    </div>
  );
};

const languages = (value) => {
  if (value === 3) {
    return "Native (broken speech)";
  }
  if (value >= 4 && value <= 5) {
    return "Native";
  }
  if (value >= 6 && value <= 8) {
    return "Native";
  }
  if (value >= 9 && value <= 12) {
    return "Native";
  }
  if (value >= 13 && value <= 15) {
    return "Native + 1 additional";
  }
  if (value >= 16 && value <= 17) {
    return "Native + 2 additional";
  }
  if (value === 18) {
    return "Native + 3 additional";
  }
};

const literacy = (value) => {
  if (value <= 5) {
    return "Illiterate";
  }
  if (value >= 6 && value <= 8) {
    return "Basic";
  }
  if (value >= 9) {
    return "Literate";
  }
};
