import React from "react";
import { numericModifier } from "./StrengthModifier";

export const WisdomModifier = ({ value }) => {
  return (
    <div className="modifier">
      <p>
        Magic Saves: {numericModifier(value) > 0 ? "+" : ""}
        {numericModifier(value)}
      </p>
    </div>
  );
};
