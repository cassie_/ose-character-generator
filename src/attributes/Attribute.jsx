import React, { useState } from "react";
import { CharismaModifier } from "./CharismaModifier";
import { ConstitutionModifier } from "./ConstitutionModifier";
import { DexterityModifier } from "./DexterityModifier";
import { IntelligenceModifier } from "./IntelligenceModifier";
import { StrengthModifier } from "./StrengthModifier";
import { WisdomModifier } from "./WisdomModifier";

const modifierSwitch = (name, value) => {
  switch (name) {
    case "Strength":
      return <StrengthModifier value={value} />;
    case "Intelligence":
      return <IntelligenceModifier value={value} />;
    case "Dexterity":
      return <DexterityModifier value={value} />;
    case "Charisma":
      return <CharismaModifier value={value} />;
    case "Wisdom":
      return <WisdomModifier value={value} />;
    case "Constitution":
      return <ConstitutionModifier value={value} />;
    default:
      return "";
  }
};

export const Attribute = ({ name, value }) => {
  const [attribute_value] = useState(value);

  return (
    <div className="attribute">
      <h3>
        {name}: {attribute_value}
      </h3>
      {modifierSwitch(name, attribute_value)}
    </div>
  );
};
