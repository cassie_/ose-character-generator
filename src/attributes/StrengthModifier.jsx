import React from "react";

export const StrengthModifier = ({ value }) => {
  return (
    <div className="modifier">
      <p>
        Melee: {numericModifier(value) > 0 ? "+" : ""}
        {numericModifier(value)}
      </p>
      <p>
        Open doors:{" "}
        {numericModifier(value) + 2 > 0 ? 2 + numericModifier(value) : 1}-in-6
      </p>
    </div>
  );
};

export const numericModifier = (value) => {
  if (value === 3) {
    return -3;
  }
  if (value >= 4 && value <= 5) {
    return -2;
  }
  if (value >= 4 && value <= 5) {
    return -2;
  }
  if (value >= 6 && value <= 8) {
    return -1;
  }
  if (value >= 9 && value <= 12) {
    return 0;
  }
  if (value >= 13 && value <= 15) {
    return 1;
  }
  if (value >= 16 && value <= 17) {
    return 2;
  }
  if (value === 18) {
    return 3;
  }
};
