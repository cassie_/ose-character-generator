import React from "react";
import { numericModifier } from "./StrengthModifier";

export const ConstitutionModifier = ({ value }) => {
  return (
    <div className="modifier">
      <p>
        Hit Points: {numericModifier(value) > 0 ? "+" : ""}
        {numericModifier(value)}
      </p>
    </div>
  );
};
