import React from "react";
import { numericModifier } from "./StrengthModifier";

export const DexterityModifier = ({ value }) => {
  return (
    <div className="modifier">
      <p>AC: {numericModifier(value)}</p>
      <p>Missile: {numericModifier(value)}</p>
      <p>Initiative: {missile(value)}</p>
    </div>
  );
};

const missile = (value) => {
  if (value === 3) {
    return -2;
  }
  if (value >= 4 && value <= 8) {
    return -1;
  }
  if (value >= 9 && value <= 12) {
    return 0;
  }
  if (value >= 13 && value <= 17) {
    return "+1";
  }
  if (value === 18) {
    return "+2";
  }
};
