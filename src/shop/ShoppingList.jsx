import React from "react";

export const ShoppingList = ({ items, removeItem }) => {
  return (
    <div className="purchased_items">
      <h3>Your Shoppinge Liste</h3>
      <div>
        {items.map((item, index) => {
          return (
            <div
              key={index}
              className="item"
              onClick={(e) => removeItem(e, item)}
            >
              <p>
                {item.name}, {item.price} gp
              </p>
            </div>
          );
        })}
      </div>
    </div>
  );
};
