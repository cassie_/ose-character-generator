import React, { useState } from "react";
import "./GearList.css";

export const AdventuringGear = ({ gear, purchase, gold }) => {
  const [display, toggle] = useState(true);

  return (
    <div className="gear_list">
      <h3 onClick={() => toggle(!display)}>
        Adventuring Gear (click to collapse)
      </h3>
      {display
        ? gear.map((item, index) => {
            if (item.price > gold) {
              return "";
            }
            return (
              <div
                key={index}
                className="item"
                onClick={(e) => purchase(e, item)}
              >
                <p className="green">
                  {item.name}: {item.price} gp
                </p>
              </div>
            );
          })
        : ""}
    </div>
  );
};
