import React, { useState } from "react";
import { EquipmentList } from "../equipment_list";
import { AdventuringGear } from "./AdventuringGear";
import { ArmorList } from "./ArmorList";
import "./Shop.css";
import { ShoppingList } from "./ShoppingList";
import { WeaponList } from "./WeaponList";

export const Shop = ({ character_class, startingGold }) => {
  const [availableEquipment] = useState(new EquipmentList(character_class));
  const [display, toggle] = useState(true);
  const [gold, updateGold] = useState(startingGold);
  const [shoppingList, updateShoppingList] = useState([]);

  const purchase = (e, item) => {
    if (item.price > gold) {
      return false;
    }
    updateGold(gold - item.price);
    updateShoppingList([...shoppingList, item]);
  };

  const removeShoppingListItem = (e, itemToRemove) => {
    let newShoppingList = [...shoppingList];
    let foundIndex = newShoppingList.lastIndexOf(itemToRemove, 0);

    if (foundIndex !== -1) {
      // Remove the item by generating a new array without it
      newShoppingList = [
        ...newShoppingList.slice(0, foundIndex),
        ...newShoppingList.slice(foundIndex + 1),
      ];
    }
    // Return the new array
    updateShoppingList([...newShoppingList]);
    updateGold(gold + itemToRemove.price);
  };

  return (
    <div className="shop">
      <h2 onClick={() => toggle(!display)}>
        Ye Olde Equipmentshoppe (click to collapse)
      </h2>
      {display ? (
        <div>
          <h3 className="gold">Gold: {gold}</h3>
          <div className="shoppes">
            <AdventuringGear
              gear={availableEquipment.adventuring_gear}
              purchase={purchase}
              gold={gold}
            />
            <WeaponList
              weapons={availableEquipment.weapons}
              purchase={purchase}
              gold={gold}
            />
            <ArmorList
              gear={availableEquipment.armor}
              purchase={purchase}
              gold={gold}
            />
          </div>
          <ShoppingList
            items={shoppingList}
            removeItem={removeShoppingListItem}
          />
        </div>
      ) : (
        ""
      )}
    </div>
  );
};
