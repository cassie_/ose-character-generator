import React, { useState } from "react";

export const ArmorList = ({ gear, purchase, gold }) => {
  const [display, toggle] = useState(true);

  return (
    <div className="armor_list">
      <h3 onClick={() => toggle(!display)}>Armor (click to collapse)</h3>
      {display
        ? gear.map((item, index) => {
            if (item.price > gold) {
              return "";
            }
            return (
              <div
                key={index}
                className="item"
                onClick={(e) => purchase(e, item)}
              >
                <p className="green">
                  {item.name}, AC: {item.ac}: {item.price} gp
                </p>
              </div>
            );
          })
        : ""}
    </div>
  );
};
