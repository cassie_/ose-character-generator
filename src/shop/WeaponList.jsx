import React, { useState } from "react";
import "./GearList.css";

export const WeaponList = ({ weapons, purchase, gold }) => {
  const [display, toggle] = useState(true);

  return (
    <div className="gear_list">
      <h3 onClick={() => toggle(!display)}>Weapons (click to collapse)</h3>
      {display
        ? weapons.map((item, index) => {
            if (item.price > gold) {
              return "";
            }
            return (
              <div
                key={index}
                className="item"
                onClick={(e) => purchase(e, item)}
              >
                <p className="green">
                  {item.name}, {item.damage}: {item.price} gp
                </p>
              </div>
            );
          })
        : ""}
    </div>
  );
};
