const adventuring_gear = [
  { name: "Backpack", price: 5 },
  { name: "Crowbar", price: 10 },
  { name: "Garlic", price: 5 },
  { name: "Grappling Hook", price: 25 },
  { name: "Hammer (small)", price: 2 },
  { name: "Holy Symbol", price: 25 },
  { name: "Holy Water (vial)", price: 25 },
  { name: "Iron spikes (12)", price: 1 },
  { name: "Lantern", price: 10 },
  { name: "Mirror (hand-sized, steel)", price: 5 },
  { name: "Oil (1 flask)", price: 2 },
  { name: "Pole (10' long, wooden)", price: 1 },
  { name: "Rations (iron, 7 days)", price: 15 },
  { name: "Rations (standard, 7 days)", price: 5 },
  { name: "Rope (50')", price: 1 },
  { name: "Sack (small)", price: 1 },
  { name: "Sack (large)", price: 2 },
  { name: "Stakes (3) and mallet", price: 3 },
  { name: "Thieves' tools", price: 25 },
  { name: "Tinder box (flint & steel)", price: 3 },
  { name: "Torches (6)", price: 1 },
  { name: "Waterskin", price: 1 },
  { name: "Wolfsbane", price: 10 }
]

const blunt_weapons = [
  { name: "Club", price: 3, damage: "1d4" },
  { name: "Mace", price: 30, damage: "1d6" },
  { name: "Warhammer", price: 5, damage: "1d6" },
  { name: "Sling", price: 2, damage: "1d4" },
  { name: "Staff", price: 2, damage: "1d4" }
]

const other_weapons = [
  { name: "Battle axe", price: 7, damage: "1d8" },
  { name: "Crossbow", price: 30, damage: "1d6" },
  { name: "Dagger", price: 3, damage: "1d4" },
  { name: "Hand axe", price: 4, damage: "1d6" },
  { name: "Holy water vial", price: 1, damage: "1d8" },
  { name: "Oil flask, burning", price: 1, damage: "1d10" },
  { name: "Javelin", price: 1, damage: "1d4" },
  { name: "Lance", price: 5, damage: "1d6" },
  { name: "Long bow", price: 40, damage: "1d6" },
  { name: "Polearm", price: 7, damage: "1d10" },
  { name: "Short bow", price: 25, damage: "1d6" },
  { name: "Short sword", price: 7, damage: "1d6" },
  { name: "Silver dagger", price: 30, damage: "1d4" },
  { name: "Spear", price: 4, damage: "1d6" },
  { name: "Sword", price: 10, damage: "1d8" },
  { name: "Torch", price: 1, damage: "1d4" },
  { name: "Two-handed sword", price: 15, damage: "1d10" },
]

const armor = [
  { name: "Leather", ac: "7 [12]", price: 20 },
  { name: "Chainmail", ac: "5 [14]", price: 40 },
  { name: "Plate mail", ac: "3 [16]", price: 60 },
  { name: "Shield", ac: "+1 bonus", price: 10 }
]

const weapons_list = (name) => {
  switch (name) {
    case "Cleric":
    default:
      return blunt_weapons.concat(other_weapons)
  }
}

export class EquipmentList {
  constructor(class_name) {
    this.adventuring_gear = adventuring_gear;
    this.weapons = weapons_list(class_name)
    this.armor = armor;
  }
}