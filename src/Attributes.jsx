import React, { useState } from "react";
import "./Attributes.css";
import { Attribute } from "./attributes/Attribute";

export const Attributes = ({ character }) => {
  const [display, toggle] = useState(false);

  return (
    <div>
      <div className="header">
        <h2>{character._class.class_name}</h2>
        <h2>HP: {character._class.hp}</h2>
      </div>
      <h2 onClick={() => toggle(!display)}>
        Attributes (click to {display ? "collapse" : "show"})
      </h2>
      {display ? (
        <div className="attributes">
          <Attribute name={"Strength"} value={character.attributes.strength} />
          <Attribute
            name={"Intelligence"}
            value={character.attributes.intelligence}
          />
          <Attribute name={"Wisdom"} value={character.attributes.wisdom} />
          <Attribute
            name={"Dexterity"}
            value={character.attributes.dexterity}
          />
          <Attribute
            name={"Constitution"}
            value={character.attributes.constitution}
          />
          <Attribute name={"Charisma"} value={character.attributes.charisma} />
        </div>
      ) : (
        ""
      )}
    </div>
  );
};
