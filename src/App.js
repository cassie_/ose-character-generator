import React, { useState } from 'react';
import './App.css';
import { Attributes } from './Attributes';
import { Character } from './character_generator';
import { Shop } from './shop/Shop';

function App() {
  const [character] = useState(new Character());

  return (
    <div className="App">
      <header className="App-header">
        <h1>An OSE Chargen</h1>
      </header>

      <div>
        <Attributes character={character} />
        <Shop character_class={character._class.class_name} startingGold={character.gold} />
      </div>
    </div>
  );
}

export default App;
